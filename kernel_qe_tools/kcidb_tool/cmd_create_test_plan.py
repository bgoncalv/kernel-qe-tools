"""Subcommand create."""

import sys

from cki_lib.yaml import ValidationError

from . import cmd_misc
from . import utils
from .dataclasses import NVR
from .dataclasses import TestPlanParserArguments
from .test_plan_parser import TestPlanParser


def build(cmds_parser, common_parser):
    """Build the argument parser for the create command."""
    # pylint: disable=duplicate-code
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "create-test-plan",
        help_message='Create a simple test plan only with the system provision task.',
        add_subparser=False,
    )

    cmd_parser.description = 'Create a simple test plan only with the system provision task.'

    cmd_parser.add_argument('--arch',
                            type=str,
                            choices=['x86_64', 'aarch64', 'ppc64', 'ppc64le', 's390x'],
                            required=True,
                            help="The test plan's architecture.")

    cmd_parser.add_argument('-c', '--checkout',
                            type=str,
                            required=True,
                            help="The checkout name used to generate all the kcidb ids.")

    cmd_parser.add_argument('--nvr',
                            type=str,
                            required=True,
                            help="NVR info.")

    cmd_parser.add_argument('--brew-task-id',
                            type=str,
                            required=False,
                            help="Id of the Brew task where the package was compiled.")

    cmd_parser.add_argument('-d', '--debug',
                            action='store_true',
                            help="Enable it if using kernel debug build."
                            " DEPRECATED, kcidb_tool will get the information from the nvr.")

    cmd_parser.add_argument('-o', '--output',
                            type=str,
                            default='kcidb.json',
                            help="Path to the KCIDB file (By default kcidb.json).")

    cmd_parser.add_argument('--origin',
                            type=str,
                            required=False,
                            default='kcidb_tool',
                            help="The default origin for all objects (By default kcidb_tool)."
                            " It can be overwritten with builds-origin, checkout-origin or"
                            " tests-origin")

    cmd_parser.add_argument('--checkout-origin',
                            type=str,
                            required=False,
                            help="The origin for the checkout, if it's not provided will"
                            " fallback to origin")

    cmd_parser.add_argument('--builds-origin',
                            type=str,
                            required=False,
                            help="The origin for all builds, if it's not provided will"
                            " fallback to origin")

    cmd_parser.add_argument('--src-nvr',
                            type=str,
                            required=False,
                            help="NVR for the source package, nvr value will be used if it is not"
                            " provided"
                            )


def main(args):
    """Run cli command."""
    # pylint: disable=duplicate-code
    package_name, package_version, package_release = utils.get_nvr(args.nvr)
    if None in [package_name, package_version, package_release]:
        sys.exit(f'Invalid value for nvr: {args.nvr}')

    if args.debug and not package_name.endswith('-debug'):
        package_name += '-debug'

    src_nvr = args.src_nvr or args.nvr
    src_package_name, src_package_version, src_package_release = utils.get_nvr(src_nvr)
    if None in [src_package_name, src_package_version, src_package_release]:
        sys.exit(f'Invalid value for source nvr: {src_nvr}')

    external_arguments = TestPlanParserArguments(
        arch=args.arch,
        brew_task_id=args.brew_task_id,
        builds_origin=args.builds_origin or args.origin,
        checkout=args.checkout,
        checkout_origin=args.checkout_origin or args.origin,
        nvr=NVR(package_name, package_version, package_release),
        src_nvr=NVR(src_package_name, src_package_version, src_package_release),
    )

    parser = TestPlanParser(external_arguments)

    parser.process()
    try:
        parser.write(args.output)
    except ValidationError as exc:
        sys.exit(exc.message)

    print(f'File {args.output} wrote !!')
