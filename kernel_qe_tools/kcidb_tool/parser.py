"""Common parser to kcidb."""

from dataclasses import asdict
from email.utils import parseaddr
import xml.etree.ElementTree as ET

from cki_lib.kcidb import KCIDBFile
from cki_lib.kcidb import PRODUCER_KCIDB_SCHEMA
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb.validate import validate_extended_kcidb_schema
from cki_lib.logger import get_logger

from . import utils

LOGGER = get_logger(__name__)


class KCIDBToolParser:
    """Parser."""

    # pylint: disable=too-many-instance-attributes
    DEFAULT_TEST_LOG_FILENAME = 'UNDEFINED'

    def __init__(self, test_runner_content, args):
        """Initialize object with the given data."""
        self.root = ET.fromstring(test_runner_content)
        self.args = args
        self.default_maintainers = self.get_default_maintainers()
        self.report = None
        self.arch = None
        self.checkout_origin = utils.slugify(self.args.checkout_origin)
        self.builds_origin = utils.slugify(self.args.builds_origin)
        self.tests_origin = utils.slugify(self.args.tests_origin)
        self.processed = False
        self._clean_report()

    def exist_build_by_arch(self):
        """Check if the build by architecture has been previously stored."""
        for build in self.report['builds']:
            if build.get('id') == self._get_build_id():
                return True
        return False

    @property
    def checkout_id(self):
        """Get checkout id."""
        checkout = utils.slugify(self.args.checkout)
        return f'{self.checkout_origin}:{checkout}'

    def _get_build_id(self):
        """Generate the build id given an architecture."""
        arch = utils.slugify(self.arch)
        nvr_name = utils.slugify(self.args.nvr.name)
        checkout_id_without_origin = self.checkout_id.split(':')[1]
        return f'{self.builds_origin}:{checkout_id_without_origin}_{arch}_{nvr_name}'

    def _get_test_id(self, test_id):
        """Generate test_case_id."""
        build_id_without_origin = self._get_build_id().split(':')[1]
        return f'{self.tests_origin}:{build_id_without_origin}_kcidb_tool_{test_id}'

    def _clean_report(self):
        """Clean report."""
        self.processed = False
        self.report = {
            'checkout': None,
            'builds': [],
            'tests': []
        }

    def get_extra_output_files(self):
        """Append output passed by cli."""
        return [asdict(output_file) for output_file in self.args.extra_output_files]

    def add_checkout_misc_info(self):
        """Return misc for a checkout."""
        misc = {}

        misc['is_public'] = False
        misc['kernel_version'] = f'{self.args.nvr.version}-{self.args.nvr.release}'
        misc['source_package_name'] = self.args.src_nvr.name
        misc['source_package_release'] = self.args.src_nvr.release
        misc['source_package_version'] = self.args.src_nvr.version

        misc['provenance'] = []

        if self.args.brew_task_id:
            brew_task_url = utils.get_brew_url(self.args.brew_task_id)
            misc['provenance'].append(utils.get_provenance_info('executor',
                                                                brew_task_url,
                                                                'buildsystem'))
        if self.args.tests_provisioner_url:
            misc['provenance'].append(utils.get_provenance_info(
                'coordinator',
                self.args.tests_provisioner_url,
                utils.get_service_name_by_url(self.args.tests_provisioner_url)))

        misc['report_rules'] = self.args.report_rules
        misc['submitter'] = self.args.submitter

        return utils.clean_dict(misc)

    def add_build_misc_info(self):
        """Return misc for a build."""
        misc = {}

        misc['package_name'] = self.args.nvr.name
        misc['package_release'] = self.args.nvr.release
        misc['package_version'] = self.args.nvr.version
        if self.args.brew_task_id:
            brew_task_url = utils.get_brew_url(self.args.brew_task_id)
            misc['provenance'] = [utils.get_provenance_info('executor',
                                                            brew_task_url,
                                                            'buildsystem')]

        # This line looks line an error, but it is not.
        # Please review this link before changing it:
        # * https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/-/issues/46
        misc['test_plan_missing'] = self.args.test_plan

        return utils.clean_dict(misc)

    def get_default_test_log(self, output_files):
        """Search the URL of the default test file name or return None."""
        for log in output_files:
            if log['name'] == self.DEFAULT_TEST_LOG_FILENAME:
                return log['url']

        return None

    def get_default_maintainers(self):
        """Get default maintainers from CLI."""
        maintainers = []
        for contact in self.args.contacts:
            name, email = parseaddr(contact)
            maintainers.append({
                'name': name or email,
                'email': email
            })
        return maintainers

    def write(self, path):
        """Write info."""
        LOGGER.info("Preparing KCIDB file")
        kcidb_file = KCIDBFile(path)

        # Checkout
        kcidb_file.set_checkout(self.report['checkout']['id'], self.report['checkout'])

        # Builds
        for build in self.report['builds']:
            kcidb_file.set_build(build['id'], build)

        # Tests
        for test in self.report['tests']:
            kcidb_file.set_test(test['id'], test)

        LOGGER.info("Validating KCIDB file")
        try:
            validate_extended_kcidb_schema(kcidb_file.data, kcidb_schema=PRODUCER_KCIDB_SCHEMA)
        except ValidationError:
            LOGGER.debug("Invalid KCIDB data:\n%r", kcidb_file.data)
            raise

        LOGGER.info("Saving KCIDB file to %s", kcidb_file.file_path)
        kcidb_file.save()

    def process(self):
        """Process input file."""
        raise NotImplementedError

    def has_tests(self):
        """Check if the parser got tests from the input file."""
        if not self.processed:
            self.process()
        return len(self.report['tests']) > 0
