"""Test Parser."""

from importlib.resources import files
import json
import pathlib
import tempfile
import unittest
import xml.etree.ElementTree as ET

from cki_lib.yaml import ValidationError
from freezegun import freeze_time

from kernel_qe_tools.kcidb_tool.bkr.parser import Bkr2KCIDBParser
from kernel_qe_tools.kcidb_tool.bkr.utils import get_utc_datetime
from kernel_qe_tools.kcidb_tool.dataclasses import ExternalOutputFile
from kernel_qe_tools.kcidb_tool.dataclasses import NVR
from kernel_qe_tools.kcidb_tool.dataclasses import ParserArguments
from kernel_qe_tools.kcidb_tool.parser import LOGGER

ASSETS = files(__package__) / '../assets'


class TestBkrParser(unittest.TestCase):
    """Test Parser."""

    maxDiff = None

    def setUp(self):
        """Initialize tester."""
        self.bkr_content = pathlib.Path(ASSETS, 'beaker.xml').read_text(encoding='utf-8')
        self.args = ParserArguments(
            checkout='checkout_id',
            test_plan=False,
            extra_output_files=[],
            brew_task_id='47919042',
            tests_provisioner_url='https://jenkins/job/my_job/1',
            contacts=['John Doe <jdoe@redhat.com>'],
            nvr=NVR('kernel', '5.14.0', '162.2.1.164.el9'),
            src_nvr=NVR('kernel', '5.14.0', '162.2.1.164.el9'),
            builds_origin='b_origin',
            checkout_origin='c_origin',
            tests_origin='t_origin',
            report_rules='[{"when": "always", "send_to": ["ptalbert@redhat.com"]}]',
            submitter='jdoe@redhat.com'
        )
        self.parser = Bkr2KCIDBParser(self.bkr_content, self.args)

        self.parser.job_whiteboard = 'Job WhiteBoard'

        self.parser.arch = 'x86_64'

        self.parser.recipe_info = {
            'job_id': '1',
            'id': '1',
            'recipe_set_id': '1',
            'system': 'some_server',
            'whiteboard': 'Recipe WhiteBoard',
            'installation_failed': False
        }

    def test_init(self):
        """Dummy test for init."""
        self.assertEqual('c_origin:checkout_id', self.parser.checkout_id)
        self.assertCountEqual({
            'checkout': None,
            'builds': [],
            'tests': []
        }, self.parser.report)

    def test_build_id(self):
        """Test build id."""
        self.parser.add_build()
        self.assertEqual(self.parser.report['builds'][0]['id'],
                         'b_origin:checkout_id_x86_64_kernel')

    def test_exist_build_by_arch(self):
        """Test exist build by arch function works."""
        self.assertFalse(self.parser.exist_build_by_arch())

        # Add one
        self.parser.add_build()

        # Check again
        self.assertTrue(self.parser.exist_build_by_arch())

    def test_add_build(self):
        """Check add_build function."""
        expected = {
            'id': 'b_origin:checkout_id_x86_64_kernel',
            'origin': 'b_origin',
            'checkout_id': 'c_origin:checkout_id',
            'architecture': 'x86_64',
            'valid': True,
            'misc': {
                'test_plan_missing': False,
                'package_name': 'kernel',
                'package_release': '162.2.1.164.el9',
                'package_version': '5.14.0',
                'provenance': [
                    {
                        'function': 'executor',
                        # pylint: disable=line-too-long
                        'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042',  # noqa: E501
                        'service_name': 'buildsystem'
                    }
                ]
            }
        }

        # Before we don't have any build
        self.assertEqual(0, len(self.parser.report['builds']))

        # We insert one
        self.parser.add_build()
        self.assertEqual(1, len(self.parser.report['builds']))

        self.assertDictEqual(expected, self.parser.report['builds'][0])

        # If we add twice the same build_id, we only store one
        self.parser.add_build()
        self.assertEqual(1, len(self.parser.report['builds']))

    def test_add_checkout(self):
        """Check add_checkout function."""
        expected_checkout = {
            'id': 'c_origin:checkout_id',
            'origin': 'c_origin',
            'tree_name': 'rhel-8.6',
            'start_time': '2022-04-09T00:25:27+00:00',
            'valid': True,
            'misc': {
                'is_public': False,
                'kernel_version': '5.14.0-162.2.1.164.el9',
                'provenance': [
                    {
                        'function': 'executor',
                        'service_name': 'buildsystem',
                        # pylint: disable=line-too-long
                        'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042'  # noqa: E501
                    },
                    {
                        'function': 'coordinator',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'report_rules': '[{"when": "always", "send_to": ["ptalbert@redhat.com"]}]',
                'source_package_name': 'kernel',
                'source_package_release': '162.2.1.164.el9',
                'source_package_version': '5.14.0',
                'submitter': 'jdoe@redhat.com'
            }
        }

        # Empty checkout
        self.assertIsNone(self.parser.report['checkout'])

        # Adding the checkout
        self.parser.add_checkout()

        self.assertDictEqual(expected_checkout, self.parser.report['checkout'])

    def test_add_test_only_test_plan(self):
        """Check add_test method when it is a test_plan."""
        test_id = 1
        beaker_content = """
        <xml>
          <task name="task name" id="10"/>
        </xml>
        """
        self.parser.args.test_plan = True
        expected_test = {
            'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1',
            'origin': 't_origin',
            'build_id': 'b_origin:checkout_id_x86_64_kernel',
            'comment': 'task name',
            'path': 'task_name',
            'environment': {
                'comment': 'some_server'
            }
        }
        task = ET.fromstring(beaker_content).find('task')

        # Tests empty
        self.assertEqual([], self.parser.report['tests'])

        self.parser.add_test(task, test_id)

        self.assertEqual(1, len(self.parser.report['tests']))
        self.assertDictEqual(expected_test, self.parser.report['tests'][0])

    def test_add_test_no_test_plan(self):
        """Check add_test method when it is not a test plan."""
        test_ids = [1, 2]
        beaker_content = """
        <xml>
          <task name="task with results" role="None" version="1.0-11" id="1" result="Pass" status="Completed" avg_time="300" start_time="2022-04-09 00:36:26" finish_time="2022-04-09 00:37:39" duration="0:01:13">
            <fetch url="https://gitlab.com/tests/-/archive/my_branch/repo.zip#path/to/test"/>
            <logs>
              <log href="https://srv/recipes/1/tasks/1/logs/harness.log" name="harness.log"/>
              <log href="https://srv/recipes/1/tasks/1/logs/taskout.log" name="taskout.log"/>
              <log href="https://srv/recipes/1/tasks/1/logs/test.log" name="test.log"/>
              <log href="https://srv/recipes/1/tasks/1/logs/messages" name="messages"/>
            </logs>
            <results>
              <result path="result one" start_time="2022-04-09 00:37:37" score="None" result="Pass" id="1">None
                <logs>
                  <log href="https://srv/recipes/1/tasks/1/results/1/logs/dmesg.log" name="dmesg.log"/>
                  <log href="https://srv/recipes/1/tasks/1/results/1/logs/avc.log" name="avc.log"/>
                  <log href="https://srv/recipes/1/tasks/1/results/1/logs/resultoutputfile.log" name="resultoutputfile.log"/>
                </logs>
              </result>
            </results>
            <fetch url="https://gitlab.com/tests/-/archive/my_branch/repo.zip#path/to/test"/>
          </task>
          <task name="task without results" role="None" version="1.0-11" id="2" result="Fail" status="Completed" avg_time="300" start_time="2022-04-09 00:36:26" finish_time="2022-04-09 00:37:39" duration="0:01:13">
            <fetch url="https://gitlab.com/tests/-/archive/my_branch/repo.zip#path/to/test"/>
            <logs>
              <log href="https://srv/recipes/1/tasks/2/logs/harness.log" name="harness.log"/>
              <log href="https://srv/recipes/1/tasks/2/logs/taskout.log" name="taskout.log"/>
              <log href="https://srv/recipes/1/tasks/2/logs/test.log" name="test.log"/>
              <log href="https://srv/recipes/1/tasks/2/logs/messages" name="messages"/>
            </logs>
          </task>
        </xml>
        """  # noqa: E501
        tasks = ET.fromstring(beaker_content).findall('task')
        expected_with_results = {
            'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1',
            'log_url': 'https://srv/recipes/1/tasks/1/logs/taskout.log',
            'origin': 't_origin',
            'build_id': 'b_origin:checkout_id_x86_64_kernel',
            'comment': 'task with results',
            'path': 'task_with_results',
            'start_time': '2022-04-09T00:36:26+00:00',
            'duration': 73,
            'output_files': [
                {'name': 'harness.log', 'url': 'https://srv/recipes/1/tasks/1/logs/harness.log'},
                {'name': 'taskout.log', 'url': 'https://srv/recipes/1/tasks/1/logs/taskout.log'},
                {'name': 'test.log', 'url': 'https://srv/recipes/1/tasks/1/logs/test.log'},
                {'name': 'messages', 'url': 'https://srv/recipes/1/tasks/1/logs/messages'},
                {
                    'name': 'console.log',
                    'url': 'https://beaker.engineering.redhat.com/recipes/1/logs/console.log'
                },
            ],
            'status': 'PASS',
            'misc': {
                'fetch_url': 'https://gitlab.com/tests/-/archive/my_branch/repo.zip#path/to/test',
                'results': [{
                    'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1.1',
                    'comment': 'result one',
                    'name': 'result one',
                    'status': 'PASS',
                    'output_files': [
                        {'name': 'dmesg.log',
                         'url': 'https://srv/recipes/1/tasks/1/results/1/logs/dmesg.log'
                         },
                        {'name': 'avc.log',
                         'url': 'https://srv/recipes/1/tasks/1/results/1/logs/avc.log'
                         },
                        {'name': 'resultoutputfile.log',
                         'url': 'https://srv/recipes/1/tasks/1/results/1/logs/resultoutputfile.log'
                         }
                    ]}
                ],
                'provenance': [
                    {
                        'function': 'executor',
                        'url': 'https://beaker.engineering.redhat.com/recipes/1',
                        'misc': {
                            'job_id': '1',
                            'job_whiteboard': 'Job WhiteBoard',
                            'recipe_id': '1',
                            'recipe_set_id': '1',
                            'recipe_whiteboard': 'Recipe WhiteBoard',
                            'task_id': '1'
                        },
                        'service_name': 'beaker'
                    },
                    {
                        'function': 'provisioner',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'maintainers': [
                    {
                        'name': 'John Doe',
                        'email': 'jdoe@redhat.com'
                    }
                ]
            },
            'environment': {
                'comment': 'some_server'
            }
        }
        expected_without_results = {
            'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_2',
            'log_url': 'https://srv/recipes/1/tasks/2/logs/taskout.log',
            'origin': 't_origin',
            'build_id': 'b_origin:checkout_id_x86_64_kernel',
            'comment': 'task without results',
            'path': 'task_without_results',
            'start_time': '2022-04-09T00:36:26+00:00',
            'duration': 73,
            'output_files': [
                {'name': 'harness.log', 'url': 'https://srv/recipes/1/tasks/2/logs/harness.log'},
                {'name': 'taskout.log', 'url': 'https://srv/recipes/1/tasks/2/logs/taskout.log'},
                {'name': 'test.log', 'url': 'https://srv/recipes/1/tasks/2/logs/test.log'},
                {'name': 'messages', 'url': 'https://srv/recipes/1/tasks/2/logs/messages'},
                {
                    'name': 'console.log',
                    'url': 'https://beaker.engineering.redhat.com/recipes/1/logs/console.log'
                },

            ],
            'status': 'FAIL',
            'misc': {
                'fetch_url': 'https://gitlab.com/tests/-/archive/my_branch/repo.zip#path/to/test',
                'provenance': [
                    {
                        'function': 'executor',
                        'url': 'https://beaker.engineering.redhat.com/recipes/1',
                        'misc': {
                            'job_id': '1',
                            'job_whiteboard': 'Job WhiteBoard',
                            'recipe_id': '1',
                            'recipe_set_id': '1',
                            'recipe_whiteboard': 'Recipe WhiteBoard',
                            'task_id': '2'
                        },
                        'service_name': 'beaker'
                    },
                    {
                        'function': 'provisioner',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'maintainers': [
                    {
                        'name': 'John Doe',
                        'email': 'jdoe@redhat.com'
                    }
                ]

            },
            'environment': {
                'comment': 'some_server'
            }
        }

        # Empty tests
        self.assertEqual([], self.parser.report['tests'])

        # Insert first tests
        self.parser.add_test(tasks[0], test_ids[0])

        self.assertEqual(1, len(self.parser.report['tests']))
        self.assertDictEqual(expected_with_results, self.parser.report['tests'][0])

        # Insert second tests
        self.parser.add_test(tasks[1], test_ids[1])

        self.assertEqual(2, len(self.parser.report['tests']))
        self.assertDictEqual(expected_without_results, self.parser.report['tests'][1])

    def test_process(self):
        """Check if we can process a beaker content."""
        # In the beaker.xml we'll obtain 1 build and 24 tests and five recipes
        self.parser.args.test_plan = True
        self.parser.process()

        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(27, len(self.parser.report['tests']))

        self.parser.args.test_plan = False
        self.parser.process()

        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(27, len(self.parser.report['tests']))

    @freeze_time("2022-04-22 09:53:20", tz_offset=0)
    def test_write_no_test_plan(self):
        """Check write the final result."""
        output_file = 'kcidb.json'
        expected_kcidb_file = pathlib.Path(ASSETS, 'kcidb_beaker.json')
        self.parser.args.test_plan = False
        self.parser.args.extra_output_files = [
            ExternalOutputFile(name='job_url', url='https://jenkins/job/my_job/1'),
            ExternalOutputFile(name='brew_url', url='https://brew/some_url')
        ]
        self.parser.process()
        fake_file = pathlib.Path('/tmp/', 'kcidb_all.json')
        self.parser.write(fake_file)
        with tempfile.TemporaryDirectory() as tmpdir:
            kcidb_file = pathlib.Path(tmpdir, output_file)
            self.parser.write(kcidb_file)

            computed = json.loads(kcidb_file.read_text(encoding='utf-8'))
            expected = json.loads(expected_kcidb_file.read_text(encoding='utf-8'))

        for obj_type, expected_values in expected.items():
            # Skip version checking
            if obj_type == "version":
                continue

            self.assertEqual(expected_values, computed[obj_type])

    def test_get_tree_name(self):
        """Test to get the tree name using the distro field."""
        # Test with the final beaker file
        expected_tree_name = 'rhel-8.6'
        self.assertEqual(expected_tree_name, self.parser.get_tree_name())

        # Test with recpies without distro field
        bkr_content = """
        <recipeSet priority="Normal" response="ack" id="1">
            <recipe id="1" job_id="1" recipe_set_id="1" result="Pass" status="Completed"/>
            <recipe id="2" job_id="2" recipe_set_id="1" result="Pass" status="Completed"/>
        </recipeSet>
        """
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        self.assertIsNone(parser.get_tree_name())

        # Test with recpies with distro field which does not match
        bkr_content = """
        <recipeSet priority="Normal" response="ack" id="1">
            <recipe id="1" job_id="1" recipe_set_id="1"/>
            <recipe id="2" job_id="2" recipe_set_id="1" distro="RHHEL-8.6.33333"/>
        </recipeSet>
        """
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        self.assertIsNone(parser.get_tree_name())

    def test_get_appended_output_files(self):
        """Test get_appended_output_files."""
        # Without files should return an empty list
        expected = []
        self.assertEqual(expected, self.parser.get_extra_output_files())

        # Now with values
        self.parser.args.extra_output_files = [
            ExternalOutputFile(name='job_url', url='https://jenkins/job/my_job/1'),
            ExternalOutputFile(name='brew_url', url='https://brew/some_url')

        ]
        expected = [
            {'name': 'job_url', 'url': 'https://jenkins/job/my_job/1'},
            {'name': 'brew_url', 'url': 'https://brew/some_url'}
        ]
        self.assertEqual(expected, self.parser.get_extra_output_files())

    def test_check_tests_with_the_same_name(self):
        """We can have the same test in different recipes."""
        test_name = 'check-install'
        expected = 5
        self.parser.process()

        result = 0
        # Getting all the test with check_install name
        for test in self.parser.report['tests']:
            if test_name in test['path']:
                result += 1

        self.assertEqual(expected, result)

    def test_get_default_maintainers(self):
        """Ensure get_default_maintainers works."""
        with_name = 'John Doe <jdoe@redhat.com>'
        without_name = 'jdoe@redhat.com'
        expected_with_name = [{'name': 'John Doe', 'email': 'jdoe@redhat.com'}]
        expected_without_name = [{'name': 'jdoe@redhat.com', 'email': 'jdoe@redhat.com'}]

        cases = (
            ('Email and email', with_name, expected_with_name),
            ('Only with email', without_name, expected_without_name),
        )

        for (description, args_maintainers, expected) in cases:
            with self.subTest(description):
                self.parser.args.contacts = [args_maintainers]
                self.assertListEqual(expected, self.parser.get_default_maintainers())

    def test_get_oldest_start_time_with_recipes_with_start_time(self):
        """Ensure get_oldest_start_time works with normal recipes."""
        self.assertEqual(get_utc_datetime('2022-04-09 00:25:27'),
                         self.parser.get_oldest_start_time())

    def test_get_oldest_start_time_with_recipes_without_start_time(self):
        """Ensure get_oldest_start_time works with recipes without start_time."""
        bkr_content = pathlib.Path(ASSETS,
                                   'beaker_without_start_time.xml').read_text(encoding='utf-8')
        self.parser = Bkr2KCIDBParser(bkr_content, self.args)

        self.assertIsNone(self.parser.get_oldest_start_time())

    def test_i386_is_replaced_by_i686(self):
        """Ensure there is no i386 in arch."""
        bkr_content = """
        <job id="10" owner="kernel-team@redhat.com" result="Fail" status="Aborted">
          <whiteboard>Job White Board</whiteboard>
          <recipeSet priority="Normal" response="ack" id="23">
            <recipe id="33" job_id="10" recipe_set_id="23" whiteboard="WhiteBoard 33"
             role="None" ks_meta="method=http selinux=--enforcing
             harness='restraint-rhts beakerlib beakerlib-redhat' redhat_ca_cert"
             kernel_options="" kernel_options_post="" result="Warn" status="Aborted"
             distro="RHEL-8.6.0-20220408.3" arch="i386" family="RedHatEnterpriseLinux8"
             variant="BaseOS">
             <installation/>
             <logs/>
            </recipe>
          </recipeSet>
        </job>
        """
        expected = 'i686'
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        parser.process()

        self.assertEqual(expected, parser.report['builds'][0]['architecture'])

    def test_a_recipe_with_all_task_missed_under_the_system_provision_task(self):
        bkr_content = """
        <job id="10" owner="kernel-team@redhat.com" result="Fail" status="Cancelled">
          <whiteboard>Job White Board</whiteboard>
          <recipeSet priority="Normal" response="ack" id="23">
            <recipe id="33" job_id="10" recipe_set_id="23" whiteboard="Whiteboard 33"
             start_time="2024-07-14 04:00:31" finish_time="2024-07-14 09:36:05" duration="5:35:34"
             result="Fail" status="Completed" distro="RHEL-10.0-20240709.23" arch="x86_64"
             family="RedHatEnterpriseLinux10" variant="BaseOS"
             system="sut.redhat.com">
               <installation install_started="2024-07-14 04:03:41"
                install_finished="2024-07-14 04:07:21"/>
               <roles>
                <role value="None">
                  <system value="sut.redhat.com"/>
                </role>
               </roles>
               <logs/>
               <task name="distribution/check-install" role="STANDALONE" id="40" result="Warn"
                status="Cancelled">
                 <fetch url="https://gitlab.com/tests/archive/master.zip#check-install"/>
                 <roles>
                   <role value="STANDALONE"/>
                 </roles>
                 <logs/>
                 <results>
                   <result path="/" start_time="2024-07-15 01:06:10" score="0" result="Warn"
                    id="50">None</result>
                 </results>
               </task>
               <task name="test_a" role="STANDALONE" id="41" result="Warn" status="Cancelled">
                 <fetch url="https://gitlab.com/tests/archive/master.zip#test_a"/>
                 <roles>
                   <role value="STANDALONE"/>
                 </roles>
                 <logs/>
                 <results>
                   <result path="/" start_time="2024-07-15 01:06:10" score="0" result="Warn"
                    id="51">None</result>
                 </results>
               </task>
            </recipe>
          </recipeSet>
        </job>
        """
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        parser.process()
        # The system provision task should be ERROR
        self.assertEqual("ERROR", parser.report["tests"][0]["status"])
        # All real tasks should be MISS
        self.assertEqual('MISS', parser.report['tests'][1]['status'])
        self.assertEqual('MISS', parser.report['tests'][2]['status'])

        self.assertEqual(3, len(parser.report['tests']))

    def test_parser_processed(self):
        """Identify when the parser have been used."""
        self.assertFalse(self.parser.processed)
        self.parser.process()
        self.assertTrue(self.parser.processed)

    def test_has_tests(self):
        """Ensure has_tests works."""
        # Change processed to get False
        self.parser.processed = True
        self.assertFalse(self.parser.has_tests())
        # The parser should process the file
        self.parser.processed = False
        self.assertTrue(self.parser.has_tests())

    def test_ensure_results_stop_when_a_localwatchdog_is_found(self):
        """Ensure results stop parsing when a localwatchdog is found."""
        test_id = 1
        beaker_content = """
        <xml>
          <task name="task name" id="10" result="Warn" status="Completed">
          <results>
            <result path="some_test/10_localwatchdog" start_time="2024-09-24 23:10:27"
             score="0" result="Warn" id="11">None
              <logs>
                <log href="https://server.redhat.com/recipes/1/tasks/1/results/11/logs/file.log"
                 name="file.log"/>
            </logs>
          </result>
            <result path="/99_reboot" start_time="2024-09-24 23:12:25"
             score="0" result="Warn" id="12">None</result>
          </results>
          </task>
        </xml>
        """
        task = ET.fromstring(beaker_content).find('task')

        self.parser.add_test(task, test_id)

        results = self.parser.report['tests'][0]['misc']['results']
        self.assertEqual(1, len(results))
        self.assertIn('/10_localwatchdog', results[0]['name'])

    def test_invalid_kcidb(self):
        """Test behavior when the produced KCIDB payload is invalid."""
        bkr_content = """
        <job>
          <recipeSet>
            <recipe id="1" distro="CentOS-Stream 10" arch="x86_64" status="Completed" result="FAIL">
              <logs/>
              <installation/>
              <task name="LTP" role="STANDALONE" id="1" status="Completed" result="FAIL">
                <logs>
                  <log name="missing-href.log"/>
                </logs>
              </task>
            </recipe>
          </recipeSet>
        </job>
        """
        parser = Bkr2KCIDBParser(bkr_content, self.args)
        parser.process()
        with (
            tempfile.TemporaryDirectory() as tmpdir,
            self.assertLogs(LOGGER, level="DEBUG") as log_ctx,
            self.assertRaisesRegex(
                ValidationError,
                r"Failed to validate against the KCIDB schema\. None is not of type 'string'.*",
            ),
        ):
            kcidb_file = pathlib.Path(tmpdir, "kcidb_all.json")
            parser.write(kcidb_file)

        self.assertIn(f"DEBUG:{LOGGER.name}:Invalid KCIDB data:\n", "\n".join(log_ctx.output))
